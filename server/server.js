const express = require('express');
const bodyParser = require('body-parser');
const JsonDB = require('node-json-db');
const cors = require('cors');

const db = new JsonDB("nodes", true, false);
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

let nodes = db.getData('/nodes');
const nodesHash = str => str
    ? str.split('').reduce((a, b) => {
        a = ((a << 5) - a) + b.charCodeAt(0);
        return a & a
    }, 0)
    : null;

app.get('/', (req, res) => res.send('Server API'));

//get all nodes

app.get('/nodes', (req, res) => res.send(nodes));

//get hash

app.get('/hash/:name', (req, res) => res.send({hash: nodesHash(req.params.name).toString()}));

//get current node

app.get('/nodes/:hash', (req, res) => {
    const node = nodes.find(node => node.hash === req.params.hash);

    res.send(node);
});

//delete node

app.delete('/nodes/:hash', (req, res) => {
    const nodes = db.getData('/nodes');
    const nodeIndex = nodes.findIndex(node => node.hash === req.params.hash);

    db.delete(`/nodes[${nodeIndex}]`);

    res.send(nodes);
});

//add new node

app.post('/nodes', (req, res) => {
    const node = {
        hash: nodesHash(req.body.name).toString(),
        name: req.body.name,
        connectionName: req.body.connectionName ? req.body.connectionName : null
    };

    db.push('/nodes[]', node, true);
    res.send(nodes);
});

//edit node

app.put('/nodes/:hash', (req, res) => {
    const nodeIndex = nodes.findIndex(node => node.hash === req.params.hash);

    if (nodeIndex >= 0) {
        db.delete(`/nodes[${nodeIndex}]`);
    }

    const node = {
        hash: req.params.hash,
        name: req.body.name,
        connectionName: req.body.connectionName ? req.body.connectionName : null
    };

    db.push('/nodes[]', node);
    res.send(nodes);
});


app.listen(8000, () => console.log(`Listening on port 8000`));
