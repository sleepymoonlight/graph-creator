Before starting run 
###` npm install` 
from the both folders

### `1. npm run server` from the server folder

Runs the server api at (http://localhost:8000) 

### `2. npm run start` from the client folder

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

