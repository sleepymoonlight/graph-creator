import React, {Component} from 'react';
import {Switch} from 'react-router-dom';
import {Route} from 'react-router';

import Home from './scenes/home/Home';

import './App.module.less';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/home" component={Home}/>
        </Switch>
      </div>
    );
  }
}

export default App;
