import React from 'react';
import styled from 'styled-components';

const NodeStyled = styled.div`
    display: flex; 
    align-items: center;
    justify-content: center;
    margin: 5px;
    height: 75px;
    width: 75px;
    border-radius: 50%;
    background-color: #4f7a66;
    color: white;
`;

const NodeName = styled.p`
  margin: 0;
`;

const Node = ({node, onNodeClick}) => (
    <NodeStyled onClick={() => onNodeClick(node)}>
        <NodeName>{node.name}</NodeName>
    </NodeStyled>
);

export default Node;
