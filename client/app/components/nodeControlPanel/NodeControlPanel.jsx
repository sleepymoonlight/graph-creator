import React from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from "redux";
import {connect} from 'react-redux';
import styled from 'styled-components';

import * as nodesActions from '../../actions/nodesActions';

import Form from '../form/Form';
import Button from '../button/Button';

const NodePanelStyled = styled.div`
  display: ${props => props.displayed ? "block" : "none"};
  width: 300px;
  height: 300px;
  padding: 10px;
  box-shadow: -5px 0 5px -5px #333;
`;

const ButtonsContainer = styled.div`
  display: flex;
  padding-bottom: 15px;
  justify-content: space-between;
`;


class NodeControlPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hash: ''
        }
    }

    onSubmit = (name, connectionName) => {
        const {nodes, isGraphNew, actions, currentNode, closeControlPanel} = this.props;
        const isNodeAlreadyExist = nodes.nodes.some(node => node.name === name);

        if (!isNodeAlreadyExist && name) {
            const newNode = {
                name: name,
                connectionName: connectionName
            };

            if (isGraphNew) {
                actions.addNewNode(newNode);
            } else {
                newNode.hash = currentNode.hash;

                actions.editCurrentNode(newNode);
            }
            closeControlPanel();
        } else {
            alert('Input another node name!');
        }
    };

    onNameChange = name => {
        this.props.actions.getCurrentHash(name);

        this.setState({hash: this.props.currentHash});
    };

    render() {
        const {onSubmit, onNameChange} = this;
        const {hash} = this.state;
        const {isGraphNew, currentNode, nodes, displayed, deleteNode, closeControlPanel} = this.props;

        const name = isGraphNew ? '' : currentNode.name;
        const connectionName = isGraphNew ? '' : currentNode.connectionName;
        const hashVal = isGraphNew ? hash : currentNode.hash;
        const availableNodes = nodes ? nodes.nodes.map(({name}) => name) : [];

        return (
            <NodePanelStyled displayed={displayed}>
                <ButtonsContainer>
                    <div onClick={() => closeControlPanel()}>
                        <Button>Cancel</Button>
                    </div>
                    {
                        !isGraphNew && (
                            <div onClick={() => deleteNode(hashVal)}>
                                <Button>Delete</Button>
                            </div>
                        )
                    }
                </ButtonsContainer>
                <Form
                    name={name}
                    connectionName={connectionName}
                    hash={hashVal}
                    onSubmit={(name, connectionHash) => onSubmit(name, connectionHash)}
                    onNameChange={name => onNameChange(name)}
                    availableNodes={availableNodes}
                />
            </NodePanelStyled>
        );
    }
}

NodeControlPanel.propTypes = {
    displayed: PropTypes.bool,
    isGraphNew: PropTypes.bool,
    closeControlPanel: PropTypes.func,
};

function mapStateToProps(state) {
    return {
        nodes: state.nodes,
        currentNode: state.nodes.currentNode,
        currentHash: state.nodes.currentHash
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...nodesActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(NodeControlPanel);
