import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Node from '../node/Node';

const NodesContainer = styled.div`
  flex: 1 1 20em;
`;


const Graphs = ({data, onNodeClick}) => (
    <NodesContainer>
        {
            data && data.map(node => <Node key={node.hash} node={node} onNodeClick={nodeObj => onNodeClick(nodeObj)}/>)
        }
    </NodesContainer>
);

Graphs.propTypes = {
    data: PropTypes.array,
    onNodeClick: PropTypes.func,
};

export default Graphs;
