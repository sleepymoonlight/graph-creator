import React from 'react';
import styled from 'styled-components';

const ButtonStyled = styled.button`
  background-color: #2B2B3A;
  color: white;
  border: none;
  border-radius: 15px;
  padding: 5px 10px;
  cursor: pointer;
  outline: none;
  box-shadow: 0 0 0 #373737;
  transition: ease 0.3s;
 
  
  &:hover {
    background-color: black;
  }
 
`;


const Button = ({children}) => <ButtonStyled>{children}</ButtonStyled>;

export default Button;
