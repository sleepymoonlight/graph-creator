import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Label = styled.label`
  margin: 5px 0;
`;

const Name = styled.p`
  margin: 0;
  padding: 5px 0 2px;
`;

const InputStyled = styled.input`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  padding: 10px 20px;
  border: 1px solid rgba(55, 55, 55, 0.75);
  border-radius: 4px;
  outline: none;
  font-family: 'Aria', sans-serif;
  background-color: ${props => props.disabled ? '#bdbdbd' : 'white'};
  
  &:focus {
    box-shadow: 5px 5px 5px rgba(55, 55, 55, 0.75);
  }
`;

const SelectStyled = styled.select`
  display: inline-block;
  width: 100%;
  box-sizing: border-box;
  padding: 10px 20px;
  border: 1px solid rgba(55, 55, 55, 0.75);
  border-radius: 4px;
  outline: none;
  font-family: 'Aria', sans-serif;
  
  &:focus {
    box-shadow: 5px 5px 5px rgba(55, 55, 55, 0.75);
  }
`;

const Input = ({description, value, onChange, disabled = false, placeholder, type, valOnChange}) => (
    <Label>
        <Name>{description}</Name>
        {
            type === 'select'
                ? <SelectStyled onChange={onChange}>
                    <option value={valOnChange ? valOnChange : ''}>{valOnChange ? valOnChange : placeholder}</option>
                    {
                        value.map(nodeName => <option key={nodeName} value={nodeName}>{nodeName}</option>)
                    }
                </SelectStyled>
                : <InputStyled
                    value={value}
                    type='text'
                    onChange={onChange}
                    placeholder={placeholder}
                    disabled={disabled}
                />
        }
    </Label>
);

Input.propTypes = {
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.array
    ]),
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    placeholder: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
};

export default Input;
