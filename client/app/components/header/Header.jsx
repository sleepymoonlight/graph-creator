import React from 'react';
import styled from 'styled-components';

const HeaderStyled = styled.div`
    flex: 1 1 20em;
    padding: 20px 0;
    font-family: "Baskerville Old Face", serif;
    text-align: center;
    font-weight: bolder;
    font-size: 24px;
`;

const Header = () => (
    <HeaderStyled>
        GRAPH CREATOR
    </HeaderStyled>
);

export default Header;
