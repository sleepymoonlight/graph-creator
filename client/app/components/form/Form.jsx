import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Input from '../input/Input';
import Button from '../button/Button';

const FormStyled = styled.div`
  width: 300px;
`;

const ButtonContainer = styled.div`
  padding: 15px 0;
  display: flex;
  justify-content: center;
`;


class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            connectionName: ''
        };
    }

    componentDidUpdate(prevProps) {
        const {name, connectionName} = this.props;

        if (name !== prevProps.name) {
            this.setState({name: name, connectionName: connectionName})
        }
    }

    onFormSubmit = event => {
        event.preventDefault();

        const {name, connectionName} = this.state;

        this.props.onSubmit(name, connectionName);

        this.setState({name: '', connectionName: ''});
    };

    onNameChange = event => {
        this.setState({name: event.target.value});
        this.props.onNameChange(this.state.name);
    };

    onConnectionChange = event => {
        this.setState({connectionName: event.target.value});
    };


    render() {
        const {name, connectionName} = this.state;
        const {availableNodes, hash} = this.props;
        return (
            <FormStyled>
                <form
                    onSubmit={event => this.onFormSubmit(event)}
                >
                    <Input
                        description='Node name:'
                        value={name}
                        onChange={event => this.onNameChange(event)}
                        placeholder='Enter node name'
                    />
                    <Input
                        description='Connection:'
                        type='select'
                        value={availableNodes}
                        onChange={event => this.onConnectionChange(event)}
                        placeholder='Select connection node name'
                        valOnChange={connectionName}
                    />
                    <Input
                        description='Hash:'
                        value={hash}
                        disabled={true}
                        placeholder='Created automatically after name input'
                    />
                    <ButtonContainer>
                        <Button type='submit'>Submit</Button>
                    </ButtonContainer>
                </form>
            </FormStyled>
        );
    }
}

Form.propTypes = {
    name: PropTypes.string,
    availableNodes: PropTypes.array,
    connectionName: PropTypes.string,
    hash: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
    onNameChange: PropTypes.func.isRequired,
};

export default Form;
