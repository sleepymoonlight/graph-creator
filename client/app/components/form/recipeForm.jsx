import React, {Fragment} from 'react';

import Button from '../button/Button';
import Input from '../input/Input';

import style from './recipeForm.module.less';

class RecipeForm extends React.Component {
    state = {
        title: '',
        description: ''
    };

    componentDidMount() {
        const {title, description} = this.props;

        this.setState({title: title, description: description})
    }

    onSubmit = event => {
        event.preventDefault();
        this.props.onSubmit(this.state.title, this.state.description);
    };

    render() {
        return (
            <Fragment>
                <form
                    className={style.form}
                    onSubmit={event => this.onSubmit(event)}
                >
                    <Input
                        description='Recipe title'
                        value={this.state.title}
                        onChange={e => this.setState({title: e.target.value})}
                        placeholder='Enter recipes title'
                    />
                    <Input
                        types='textarea'
                        description='Description'
                        value={this.state.description}
                        onChange={e => this.setState({description: e.target.value})}
                        placeholder='Description'
                    />
                    <div className={style.buttons}>
                        <Button type='cancel' href="/">Cancel</Button>
                        <Button type='submit'>Submit</Button>
                    </div>
                </form>
            </Fragment>
        );
    }
}

export default RecipeForm;
