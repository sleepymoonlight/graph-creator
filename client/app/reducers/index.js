import {combineReducers} from 'redux';
import nodes from './nodesReducers';

export default combineReducers({
    nodes
});
