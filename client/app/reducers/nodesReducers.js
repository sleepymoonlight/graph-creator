import { handleActions } from 'redux-actions';
import { fetchAll, addNode, getHash, getNode, editNode, deleteNode } from "../actions/nodesActions";

export const initialState = {
    nodes: [],
    currentNode: {},
    currentHash: null
};

export default handleActions(
    {
        [fetchAll]: (state, action) => {
            return Object.assign({}, state, {
                nodes: action.payload
            })
        },
        [addNode]: (state, action) => {
            return Object.assign({}, state, {
                nodes: action.payload
            })
        },
        [getHash]: (state, action) => {
            return Object.assign({}, state, {
                currentHash: action.payload
            })
        },
        [getNode]: (state, action) => {
            return Object.assign({}, state, {
                currentNode: action.payload
            })
        },
        [editNode]: (state, action) => {
            return Object.assign({}, state, {
                nodes: action.payload
            })
        },
        [deleteNode]: (state, action) => {
            return Object.assign({}, state, {
                nodes: action.payload
            })
        }
    },
    initialState
)
