import {createAction} from 'redux-actions';
import axios from 'axios';

export const fetchAll = createAction('FETCH_ALL');
export const addNode = createAction('ADD_NODE');
export const getHash = createAction('GET_HASH');
export const getNode = createAction('GET_NODE');
export const editNode = createAction('EDIT_NODE');
export const deleteNode = createAction('DELETE_NODE');

export function fetchAllNodes() {
    return dispatch => {
        axios.get('http://localhost:8000/nodes')
            .then(response => dispatch(fetchAll(response.data)));
    }
}

export function addNewNode(node) {
    return dispatch => {
        return axios.post('http://localhost:8000/nodes', node)
            .then(response => dispatch(addNode(response.data)))
            .catch(error => dispatch(console.log(error)));

    }
}

export function getCurrentHash(name) {
    return dispatch => {
        axios.get(`http://localhost:8000/hash/${name}`)
            .then(response => dispatch(getHash(response.data.hash)));
    }
}

export function getCurrentNode(hash) {
    return dispatch => {
        return axios.get(`http://localhost:8000/nodes/${hash}`)
            .then(response => dispatch(getNode(response.data)));
    }
}

export function editCurrentNode(editedNode) {
    return dispatch => {
        return axios.put(`http://localhost:8000/nodes/${editedNode.hash}`, editedNode)
            .then(response => dispatch(editNode(response.data)))
            .catch(error => dispatch(console.log(error)));

    }
}

export function deleteCurrentNode(hash) {
    return dispatch => {
        const postUrl = `http://localhost:8000/nodes/${hash}`;
        return axios.delete(postUrl)
            .then(response => dispatch(deleteNode(response.data)));
    }
}
