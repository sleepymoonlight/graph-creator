import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import styled from 'styled-components';

import * as nodesActions from '../../actions/nodesActions';

import Header from '../../components/header/Header';
import NodeControlPanel from '../../components/nodeControlPanel/NodeControlPanel';
import Graphs from '../../components/graph/Graphs';
import Button from '../../components/button/Button';

const Container = styled.div`
  padding: 0 20px;
`;

const HeaderBox = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  box-shadow: 0 6px 6px -6px black;
`;

const GraphBox = styled.div`
  display: flex;
`;

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isNodeControlPanelOpen: false,
            isGraphNew: true,
            openNodeHash: ''
        };
    }

    componentDidMount() {
        this.props.actions.fetchAllNodes();
    }

    addNewGraph = () => this.setState({
        isNodeControlPanelOpen: true,
        isGraphNew: true
    });

    closeControlPanel = () => this.setState({
        isNodeControlPanelOpen: false
    });

    onNodeClick = nodeObj => {
        this.props.actions.getCurrentNode(nodeObj.hash);

        this.setState({
            isNodeControlPanelOpen: true,
            isGraphNew: false,
            openNodeHash: nodeObj.hash
        });
    };

    deleteNode = hash => {
        this.props.actions.deleteCurrentNode(hash);

        this.closeControlPanel();
    };

    render() {
        const {addNewGraph, onNodeClick, closeControlPanel, deleteNode} = this;
        const {isNodeControlPanelOpen, isGraphNew} = this.state;
        const nodesList = this.props.nodes.nodes;

        return (
            <Container>
                <HeaderBox>
                    <Header/>
                    <div onClick={() => addNewGraph()}>
                        <Button>
                            Add graph
                        </Button>
                    </div>
                </HeaderBox>
                <GraphBox>
                    <Graphs
                        data={nodesList}
                        onNodeClick={nodeObj => onNodeClick(nodeObj)}
                    />
                    <NodeControlPanel
                        displayed={isNodeControlPanelOpen}
                        isGraphNew={isGraphNew}
                        closeControlPanel={() => closeControlPanel()}
                        deleteNode={hash => deleteNode(hash)}
                    />
                </GraphBox>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        nodes: state.nodes
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...nodesActions}, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
